<?php
$name = isset($_GET["name"]) ? $_GET["name"] : "emile";
$files = scandir('shared/files/');
foreach ($files as $file) {
        if ($file != '.' && $file != '..') {
            $fileParts = explode('.',$file);
            $leerlingen[] = $fileParts[0];
            }
        }
$path = 'shared/files/'.$name.'.txt';
$fileContent = file_get_contents($path);
$lijnen = array();
$lines = explode(PHP_EOL, $fileContent);      
foreach ($lines as $line) {
    $line = trim($line);
    array_push($lijnen,$line);
}
$voornaam = $lijnen[0];
$achternaam = $lijnen[1];
$geboorteDatum = new DateTime($lijnen[2]);
$geboortePlaats = $lijnen[3];
$activiteiten = explode(",",$lijnen[4]);
$foto = $lijnen[5];
?>
<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <title>6info</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-fluid center" id="header">
                <div class="container center">
                    <h1 class="display-4">ZESINFO</h1>
                    <p class="lead">Dit is de eerste php applicatie voor de leerlingen van 6info</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h3 class="display-3">Leerlingen</h3>
                </div>
            </div>
            <!-- hier komt het overzicht van de leerlingen-->
            <div class="row">
                <div class="col-xs-12">
                    <div class="list-group">
                        <?php foreach($leerlingen as $leerling): ?>
                            <div class='list-group-item list-group-item-action'>
                                <div class='row'>
                                    <div class='col-xs-8'>
                                        <a href="index.php?name=<?php echo $leerling ?>"><?php echo $leerling ?></a>
                                    </div>
                                    <div class='col-xs-2'>
                                        <a href="edit.php?name=<?php echo $leerling ?>">Edit</a>
                                    </div>
                                    <div class='col-xs-2'>
                                        <a href="delete.php?name=<?php echo $leerling ?>">Del</a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-9">
            <!-- hier komt het detail van de geselecteerde leerling -->
            <p>Naam:            <?php echo $voornaam ?></p>
            <p>achternaam:      <?php echo $achternaam ?></p>
            <p>Geboortedatum:   <?php echo $geboorteDatum->format('Y-m-d'); ?></p>
            <p>Geboorteplaats:  <?php echo $geboortePlaats ?></p>

            <p>Activiteiten:</p>            
            <?php foreach($activiteiten as $activiteit): ?>
                <ul><?php echo $activiteit; ?></ul>
                <?php endforeach; ?>
            
            <img src="<?php echo $foto?>" style="width: 50%; vertical-align: middle; border: 0; box-sizing: border-box; display=inline-block;"></img>
        </div>
    </div>
</div>

</body>